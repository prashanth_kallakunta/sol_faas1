import time
import uuid

from xpms_file_storage.file_handler import XpmsResourceFactory, XpmsResource, LocalResource
import pandas as pd
from datetime import datetime
import os
from xpms_storage.utils import get_env

def hma_csv_to_json(config=None, **objects):

    NAMESPACE = get_env("NAMESPACE", "claims-audit", False)
    DOMAIN_NAME = get_env("DOMAIN_NAME", "enterprise.xpms.ai", False)
    file_path = "minio://{0}/claimsaudit/claimsaudit-ingestfile/batches-input".format(NAMESPACE)
    xr = XpmsResource()
    minio_resource = xr.get(urn=file_path)
    if minio_resource.exists():
        all_files_list = minio_resource.list()
        files_list = [(path.filename) for path in all_files_list if ".csv" in path.fullpath]
        if len(files_list) == 0:

            return {
                "file_path": "na"
            }
        # elif len(files_list) > 1:
        #     return {
        #         'message': 'More than one file present in ' + file_path
        #     }
        else:

            csv_data = files_list[0]
            dataset = pd.DataFrame(pd.read_csv(csv_data, sep=",", header=0, index_col=False))
            start_time = int(time.time())
            file_name = "{0}_{1}_csv_to_json.json".format(str(uuid.uuid4())[:8], start_time)
            csv_minio_urn = "minio://{0}/".format(NAMESPACE) + "/claimsaudit/claimsaudit/claimsaudit-ingestfile/batches-input/" + file_name
            local_csv_path = "/tmp/" + file_name
            minio_resource = XpmsResource.get(urn=csv_minio_urn)
            # saving the json
            dataset.to_json(local_csv_path, orient="records", date_format="epoch",
                              double_precision=10, force_ascii=True, date_unit="ms", default_handler=None)

            local_res = LocalResource(key=local_csv_path)
            local_res.copy(minio_resource)
            return {"json_urn": csv_minio_urn}
    else:
        return {

        "file_path": "na"
    }
